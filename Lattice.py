class Lattice:
    def FillWithEdges(self, edges):
        self.edges = edges
        return self

    @property
    def Edges (self):
        return self.edges

    @property
    def Dots(self):
        dots = set ()
        for edge in self.edges:
            dots.add (edge.startDot)
            dots.add (edge.endDot)
        return dots