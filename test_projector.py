from unittest import TestCase
from latticegame.Projections import Projector, ProjectionSpace

class TestProjector(TestCase):
    def test_sameOrigin(self):
        ptor = Projector(ProjectionSpace(0, 0, 400, 400), ProjectionSpace(0, 0, 10, 10))
        self.assertEquals((0, 0), ptor.project((0, 0)))
        self.assertEquals((5, 5), ptor.project((200, 200)))
        self.assertEquals((10, 10), ptor.project((400, 400)))

    def test_differentPositiveOrigin(self):
        ptor = Projector(ProjectionSpace(200, 200, 400, 400), ProjectionSpace(0, 0, 10, 10))
        self.assertEquals((0, 0), ptor.project((200, 200)))
        self.assertEquals((5, 5), ptor.project((300, 300)))
        self.assertEquals((10, 10), ptor.project((400, 400)))

    def test_differentPositiveOriginForOutspace(self):
        ptor = Projector(ProjectionSpace(0, 0, 400, 400), ProjectionSpace(5, 5, 15, 15))
        self.assertEquals((5, 5), ptor.project((0, 0)))
        self.assertEquals((10, 10), ptor.project((200, 200)))
        self.assertEquals((15, 15), ptor.project((400, 400)))

    def test_differentPositiveOriginMixed(self):
        ptor = Projector(ProjectionSpace(200, 200, 400, 400), ProjectionSpace(5, 5, 15, 15))
        self.assertEquals((5, 5), ptor.project((200, 200)))
        self.assertEquals((10, 10), ptor.project((300, 300)))
        self.assertEquals((15, 15), ptor.project((400, 400)))

    def test_negativeOrigins(self):
        ptor = Projector(ProjectionSpace(0, 0, 400, 400), ProjectionSpace(-10, -10, 10, 10))
        self.assertEquals((-10, -10), ptor.project((0, 0)))
        self.assertEquals((0, 0), ptor.project((200, 200)))
        self.assertEquals((10, 10), ptor.project((400, 400)))

    def test_verticalMirroredSpaces(self):
        ptor = Projector(ProjectionSpace(0, 0, 400, 400), ProjectionSpace(-10, -10, 10, 10).VerticalMirror())
        self.assertEquals((-10, 10), ptor.project((0, 0)))
        self.assertEquals((0, 0), ptor.project((200, 200))) # prob
        self.assertEquals((10, -10), ptor.project((400, 400)))

    def test_firstNegativeVerticalSpaceToPositiveSpace(self):
        ptor = Projector(ProjectionSpace(-10, -10, 10, 10).VerticalMirror(), ProjectionSpace(0, 0, 400, 400))
        self.assertEquals((0, 0), ptor.project((-10, 10)))
        self.assertEquals((400, 400), ptor.project((10, -10))) #prob
        self.assertEquals((200, 200), ptor.project((0, 0)))

