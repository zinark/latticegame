from latticegame.Comparable import Comparable
from latticegame.Lattices import Lattice, Stick, LatticeTransformer

class Pattern(Comparable):
    def __init__(self, name, lattice, score=50, imageurl='/static/images/spiderman.png'):
        assert isinstance(lattice, Lattice)
        assert isinstance(score, int)
        assert isinstance(imageurl, str)
        self.lattice = lattice
        self.score = score
        self.imageurl = imageurl
        self.name = name

    def _compare(self, other, method):
        return self.score

    def __repr__(self):
        return '{0}, {1}'.format(self.name, self.score)


class PatternLoader:
    def Load(self):
        latticeSquare = Lattice()\
        .addStick(Stick((0, 0), (1, 0)))\
        .addStick(Stick((1, 0), (1, 1)))\
        .addStick(Stick((1, 1), (0, 1)))\
        .addStick(Stick((0, 1), (0, 0)))

        latticeTri = Lattice()\
        .addStickFromPos(0, 0, 1, 1)\
        .addStickFromPos(1, 1, 2, 0)\
        .addStickFromPos(2, 0, 1, 0)\
        .addStickFromPos(1, 0, 0, 0)

        return [Pattern('square', latticeSquare, score=50), Pattern('triangle', latticeTri, score=75)]


class PatternFinder:
    def __init__(self, patterns):
        self.patterns = patterns

    @staticmethod
    def Exists(lattice, pos, pattern):
        assert isinstance(pattern, Pattern)
        assert isinstance(lattice, Lattice)
        transformedPatternLattice = LatticeTransformer(pattern.lattice).transform(pos)

        # it can be optimized by pattern width and height. Lattice needs width and height by .Positions () : ()[]

        for stick in transformedPatternLattice.sticks:
            logic1 = stick not in lattice.sticks
            logic2 = stick.reverse() not in lattice.sticks
            #            print '{0} not in {1} = {2}'.format(stick, lattice.sticks, logic1)
            #            print '{0} not in {1} = {2}'.format(stick.reverse(), lattice.sticks, logic1)
            if logic1 and logic2:
                return False

        return True

    def Find(self, lattice):
        patternsByPos = {}

        for pattern in sorted(self.patterns, reverse=True):
            for stick in lattice.sticks:
                if PatternFinder.Exists(lattice, stick.startPos, pattern):
                    patternsByPos[stick.startPos] = pattern
        return patternsByPos