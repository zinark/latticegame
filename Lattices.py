from latticegame.Comparable import Comparable

class Stick (Comparable):
    def __init__(self, startPos, endPos):
        assert isinstance(startPos, tuple)
        assert isinstance(endPos, tuple)
        assert len(startPos) == 2
        assert len(endPos) == 2
        assert startPos != endPos, 'positions should be different!'
        self.startPos = startPos
        self.endPos = endPos
        self.relatedSticks = []

    def comparerKey(self):
        return self.startPos, self.endPos


    def reverse (self):
        return Stick (self.endPos, self.startPos)

    def __repr__(self):
        return '({0},{1})'.format(self.startPos, self.endPos)

    def addRelatedStick (self, stick):
        self.relatedSticks.append (stick)


class Lattice(object):
    def __init__(self):
        self.sticks = []
        self.id = 0

    def positions (self):
        pos = set ()
        for stick in self.sticks:
            pos.add(stick.startPos)
            pos.add(stick.endPos)
        return pos

    def addStick(self, stick):
        self.sticks.append(stick)
        relatedSticks = list (set (self.findSticksAt(stick.endPos) + self.findSticksAt (stick.startPos)))
        relatedSticks.remove(stick)
#        print 'startPos', stick.startPos, ' -> ', self.findSticksAt(stick.startPos)
#        print 'endPos', stick.endPos, ' -> ', self.findSticksAt(stick.endPos)
#        print 'new stick related sticks', relatedSticks
        stick.relatedSticks = relatedSticks
        return self

    def addStickFromPos(self, x0, y0, x1, y1):
        self.addStick(Stick((x0, y0), (x1, y1)))
        return self

    def findSticksAt (self, pos):
        return [s for s in self.sticks if s.startPos == pos or s.endPos == pos]

    def linkedSticks (self, pos, visitedDots = list()):
        result = []
        visitedDots.append (pos)
        relatedSticks = self.findSticksAt (pos)

        dots = set()
        for stick in relatedSticks:
            dots.add(stick.endPos)
            dots.add(stick.startPos)
            result.append(stick)

        for dot in visitedDots:
            if dot in dots: dots.remove(dot)

        for dot in dots:
            innerLinkedSticks = self.linkedSticks(dot, visitedDots)
            result += innerLinkedSticks

        return list(set(result)) # TODO : We got a problem here... multiplication?

    def destroySticks(self, destroyPos, strength):
        latticeStrength = len (self.sticks)
        if strength > latticeStrength: strength = latticeStrength

        linkedSticks = self.linkedSticks(destroyPos)
        toDestroySticks = linkedSticks[:strength]
        for stick in toDestroySticks:
            print 'removing stick', stick
            self.sticks.remove(stick)
        return self

    @property
    def homePoint(self):
        firstStick = self.sticks[0]
        assert isinstance(firstStick, Stick)
        return firstStick.startPos

    @property
    def headPoint(self):
        lastStick = self.sticks[len(self.sticks) - 1]
        assert isinstance(lastStick, Stick)
        return lastStick.endPos

    def __repr__(self):
        result = ''
        for s in self.sticks:
            result += Stick.__repr__(s) + '\n'
        return result


class StickMovementValidator:
    def Validate(self, lattice, stick):
        assert isinstance(lattice, Lattice)
        assert isinstance(stick, Stick)

        if abs(stick.startPos[0] - stick.endPos[0]) > 1: return False
        if abs(stick.startPos[1] - stick.endPos[1]) > 1: return False

        isStickConnectedToLattice = False
        for s in lattice.sticks:
            if s.startPos == stick.startPos or s.startPos == stick.endPos or s.endPos == stick.startPos or s.endPos == stick.endPos: isStickConnectedToLattice = True
            if s.startPos == stick.startPos and s.endPos == stick.endPos: return False
            if s.startPos == stick.endPos and s.endPos == stick.startPos: return False

        return True and isStickConnectedToLattice

class BattleModeValidator:
    def Validate(self, fromLattice, toLattice, startPos, battlePos):
        assert isinstance(toLattice, Lattice)
        assert isinstance(fromLattice, Lattice)
        assert isinstance(battlePos, tuple)
        assert len(battlePos) == 2

        if startPos not in fromLattice.positions(): return False
        if abs(startPos[0] - battlePos[0]) > 1: return False
        if abs(startPos[1] - battlePos[1]) > 1: return False

        relatedSticks = [s for s in toLattice.sticks if s.startPos == battlePos or s.endPos == battlePos]
        return len(relatedSticks) > 0


class LatticeCropper:
    def __init__(self, lattice, fromPos, toPos):
        assert isinstance(lattice, Lattice)
        assert isinstance(fromPos, tuple)
        assert isinstance(toPos, tuple)
        assert len(fromPos) == 2
        assert len(toPos) == 2
        self.lattice = lattice
        self.fromPos = fromPos
        self.toPos = toPos

    def crop(self):
        result = Lattice()
        for stick in self.lattice.sticks:
            if stick.startPos[0] < self.fromPos[0]: continue
            if stick.startPos[0] > self.toPos[0]: continue
            if stick.endPos[0] < self.fromPos[0]: continue
            if stick.endPos[0] > self.toPos[0]: continue
            if stick.startPos[1] < self.fromPos[1]: continue
            if stick.startPos[1] > self.toPos[1]: continue
            if stick.endPos[1] < self.fromPos[1]: continue
            if stick.endPos[1] > self.toPos[1]: continue
            result.addStick(stick)
        return result


class LatticeTransformer:
    def __init__(self, lattice):
        assert isinstance(lattice, Lattice)
        self.lattice = lattice

    def transform(self, pos):
        assert isinstance(pos, tuple)
        assert len(pos) == 2
        result = Lattice()
        for stick in self.lattice.sticks:
            transformedStick = Stick(
                (stick.startPos[0] + pos[0], stick.startPos[1] + pos[1]),
                (stick.endPos[0] + pos[0], stick.endPos[1] + pos[1])
            )
            result.addStick(transformedStick)

        return result

