from unittest import TestCase
from latticegame.Lattices import Lattice, Stick, StickMovementValidator

class TestStickAddValidator(TestCase):
    def test_CanAdd(self):
        lat = Lattice().addStick(Stick((0, 0), (1, 0))).addStick(Stick((1, 0), (2, 0)))
        actual = StickMovementValidator().Validate(lat, Stick((2, 0), (3, 0)))
        self.assertEquals (True, actual)

    def test_CanNotAdd_discreteStick(self):
        lat = Lattice().addStick(Stick((0, 0), (1, 0)))
        actual = StickMovementValidator().Validate(lat, Stick((2, 0), (3, 0)))
        self.assertEquals (False, actual)

    def test_CanNotAdd_emptyLattice(self):
        lat = Lattice()
        actual = StickMovementValidator().Validate(lat, Stick((2, 0), (3, 0)))
        self.assertEquals (False, actual)
