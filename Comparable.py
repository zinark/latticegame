from latticegame.LatticeError import LatticeError

class Comparable(object):
    def _compare(self, other, method):
        try:
            return method(self.comparerKey(), other.comparerKey())
        except (AttributeError, TypeError):
            return LatticeError ('dont forget to override comparerKey')

    def comparerKey (self):
        pass

    def __lt__(self, other):
        return self._compare(other, lambda s, o: s < o)

    def __le__(self, other):
        return self._compare(other, lambda s, o: s <= o)

    def __eq__(self, other):
        return self._compare(other, lambda s, o: s == o)

    def __ge__(self, other):
        return self._compare(other, lambda s, o: s >= o)

    def __gt__(self, other):
        return self._compare(other, lambda s, o: s > o)

    def __ne__(self, other):
        return self._compare(other, lambda s, o: s != o)
