import random
from latticegame.Lattices import Lattice, Stick

class UserSession:
    def __init__(self, session, latticeRepository):
        self.session = session
        self.latticeRepository = latticeRepository

    @property
    def latticeId(self):
        if not self.session.__contains__('lattice_id'):
            self.session['lattice_id'] = 0 # wtf. it should be done in main.py

        return self.session['lattice_id']

    def changeLatticeId(self, latticeId):
        self.session['lattice_id'] = latticeId

    @property
    def lattice(self):
        def defaultLattice():
            startPoint = (random.randint(-19, 19), random.randint(-19, 19))
            endPoint = (startPoint[0] + 1, startPoint[1] + 1)
            while startPoint == endPoint:
                endPoint = (startPoint[0] + random.randint(-1, 1), startPoint[1] + random.randint(-1, 1))

            lattice = Lattice().addStick(Stick(startPoint, endPoint))
            self.latticeRepository.add(lattice)
            lattice.id = self.latticeRepository.count() + 1
            self.changeLatticeId(lattice.id)
            return lattice

        if not self.latticeId:
            return defaultLattice()
        return self.latticeRepository.get(self.latticeId)

#    def changeUserId(self, userId):
#        self.session['user_id'] = userId
#
#    @property
#    def userId(self):
#        if not self.session.__contains__ ('user_id'): self.session['user_id'] = 0 # wtf. it should be done in main.py
#        return self.session['user_id']
#
#    @property
#    def user(self):
#        def defaultUser():
#            user = User(os.urandom(32))
#            self.changeUserId(user.id)
#            return user
#
#        if not self.userId: return defaultUser()
#        return self.userRepository.get(self.userId)
