import pickle
from latticegame.Lattice import Lattice

class LatticeTextSource(object):
    def __init__(self, filename):
        assert isinstance(filename, str)
        self.filename = filename

    def Load(self):
        with open(self.filename, 'r') as f:
            lines = f.readlines()
        return self.ParseLattice(lines)

    def Save (self, lattice):
        with open (self.filename, 'wb') as f:
            pickle.dump(lattice, f)

    def ParseLattice(self, lines):
        return Lattice()


def Load(latticeSource):
    # assert type(latticeSource) in [LatticeTextSource.__class__]
    return latticeSource.Load()


def Save(latticeSource, lattice):
    assert isinstance(lattice, Lattice)
    latticeSource.Save (lattice)
    return None