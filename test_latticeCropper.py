from unittest import TestCase
from latticegame.Lattices import Lattice, Stick, LatticeCropper

class TestLatticeCropper(TestCase):
    def test_Crop(self):
        lat = Lattice ()\
        .addStick(Stick ((0,0), (0,1)))\
        .addStick(Stick ((0,1), (2,2)))\
        .addStick(Stick ((2,2), (3,3)))\
        .addStick(Stick ((3,3), (4,4)))\
        .addStick(Stick ((4,4), (5,5)))\
        .addStick(Stick ((11,11), (-11,-11)))\
        .addStick(Stick ((-11,-11), (11,11)))\
        .addStick(Stick ((-11,11), (11,-11)))\
        .addStick(Stick ((0,20), (0,-20)))\
        .addStick(Stick ((-20,0), (20,0)))\

        resultLat = LatticeCropper (lat,(-10,-10),(10,10)).crop()

        print 'Result'
        print resultLat
