from unittest import TestCase
from latticegame.Lattices import Lattice
from latticegame.Patterns import PatternFinder, PatternLoader


class TestPatternFinder(TestCase):
    def test_Find(self):
        patterns = PatternLoader().Load()
        lattice = Lattice().addStickFromPos(0, 0, 1, 1).addStickFromPos(1, 1, 2, 2)\
        .addStickFromPos(2, 2, 3, 3).addStickFromPos(3, 3, 4, 2).addStickFromPos(4, 2, 3, 2).addStickFromPos(3, 2, 2, 2)
        print PatternFinder(patterns).Find(lattice)

    def test_Exists(self):
        patternTri = PatternLoader().Load()[1]
        lattice = Lattice().addStickFromPos(0, 0, 1, 1).addStickFromPos(1, 1, 2, 2)\
        .addStickFromPos(2, 2, 3, 3).addStickFromPos(3, 3, 4, 2).addStickFromPos(4, 2, 3, 2).addStickFromPos(3, 2, 2, 2)
        self.assertEquals(True, PatternFinder.Exists(lattice, (2, 2), patternTri))
