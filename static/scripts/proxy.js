function Proxy() {

    this.addStick = function (mouse, onSuccess) {
        var startx = concateValue(mouse.lastClick[0]);
        var starty = concateValue(mouse.lastClick[1]);

        $.post('/addStick/(' + startx + ',' + starty + ')-(' + concateValue(mouse.x) + ',' + concateValue(mouse.y) + ')', function (data) {
            var result = JSON.parse(data);
            mouse.lastClickedAt(concateValue(mouse.x), concateValue(mouse.y));
            onSuccess(result);
        });
    };

    this.sync = function (onSuccess) {
        $.post('/sync', function (data) {
            if (data != null) {
                onSuccess(JSON.parse(data));
            }
        });
    };

    this.getHeadPoint = function (onSuccess) {
        $.post('/headPoint', function (data) {
            if (data != null) {
                onSuccess(JSON.parse(data));
            }
        });
    }

}