var WIDTH = 800;
var HEIGHT = 800;
var GRID = 20;
var canvas = document.getElementById('c');
var context = canvas.getContext('2d');
var FPS = 60
var SYNC_INTERVAL = 500

canvas.width = WIDTH;
canvas.height = HEIGHT;
canvas.oncontextmenu = function() {
    return false;
};
