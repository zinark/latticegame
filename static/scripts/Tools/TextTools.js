function TextWrapper(context) {
    this.context = context

    this.wrapText = function (context, text, x, y, maxWidth, lineHeight) {
        var words = text.split(" ");
        var line = "";

        for (var n = 0; n < words.length; n++) {
            var testLine = line + words[n] + " ";
            var metrics = this.context.measureText(testLine);
            var testWidth = metrics.width;
            if (testWidth > maxWidth) {
                this.context.fillText(line, x, y);
                line = words[n] + " ";
                y += lineHeight;
            }
            else {
                line = testLine;
            }
        }
        this.context.fillText(line, x, y);
    };

}