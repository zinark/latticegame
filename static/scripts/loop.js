require('/static/scripts/config.js');
require('/static/scripts/Drawers.js');
require('/static/scripts/Tools/ImageTools.js');
require('/static/scripts/Tools/TextTools.js');
require('/static/scripts/mouse.js');
require('/static/scripts/proxy.js');

var syncResult = null;
var mouse = new Mouse();
var proxy = new Proxy();

canvas.onmousedown = function (e) {
    if (e.button == 2) {
        x = e.pageX - this.offsetLeft;
        y = e.pageY - this.offsetTop;
        mouse.lastClickedAt(x, y);
    }
    if (e.button == 0) {
        proxy.addStick(mouse, function (result) {
            syncResult = result;
        });
    }
};

canvas.onmousemove = function (e) {
    x = e.pageX - this.offsetLeft;
    y = e.pageY - this.offsetTop;
    mouse.setCurrent(x, y);
};

var concateValue = function (value) {
    return parseInt(value / GRID) * GRID;
};

var renderView = function () {
    new BackgroundDrawer(context).draw(true);

    if (syncResult != null) {
        for (var i in syncResult) {
            var syncItem = syncResult[i];
            if (syncItem == 'null') continue;
            new StickDrawer(context).draw(syncItem['pos'], syncItem['name']);
            new HomePointDrawer(context).draw(syncItem['home'], syncItem['name']);
            new HeadPointDrawer(context).draw(syncItem['head'], syncItem['name']);
        }
    }

    new CursorDrawer(context).draw();
}

var SyncLoop = function () {
    proxy.sync(function (result) {
        syncResult = result;
        renderView();
    });
    setTimeout(SyncLoop, SYNC_INTERVAL)
};

var GameLoop = function () {
    renderView();
    setTimeout(GameLoop, 1000 / FPS);
};

proxy.getHeadPoint (function (homePoint) {
    mouse.lastClickedAt(homePoint[0],homePoint[1]);
});

SyncLoop();
GameLoop();