var enableGlow = function (context, color, blur) {
    context.shadowColor = color;
    context.shadowOffsetX = 0;
    context.shadowOffsetY = 0;
    context.shadowBlur = blur;
};

var disableGlow = function (context) {
    context.shadowBlur = 0;
};

var howManyCircles = 15, circles = [];
for (var i = 0; i < howManyCircles; i++) {
    circles.push([Math.random() * WIDTH, Math.random() * HEIGHT, Math.random() * 100, Math.random() / 2]);
}

var DrawCircles = function () {
    for (var i = 0; i < howManyCircles; i++) {
        context.fillStyle = 'rgba(255, 255, 255, ' + circles[i][3] + ')';
        context.beginPath();
        context.arc(circles[i][0], circles[i][1], circles[i][2], 0, Math.PI * 2, true);
        context.closePath();
        context.fill();
    }
};

var MoveCircles = function (deltaY) {
    for (var i = 0; i < howManyCircles; i++) {
        if (circles[i][1] - circles[i][2] > HEIGHT) {
            circles[i][0] = Math.random() * WIDTH;
            circles[i][2] = Math.random() * 100;
            circles[i][1] = 0 - circles[i][2];
            circles[i][3] = Math.random() / 2;
        } else {
            circles[i][1] += deltaY;
        }
    }
};

function BackgroundDrawer(context) {
    this.context = context;
    this.draw = function (showGrid) {
        this.context.fillStyle = '#111111';
        this.context.beginPath();
        this.context.rect(0, 0, WIDTH, HEIGHT);
        this.context.closePath();
        this.context.fill();

        if (!showGrid) return;

        const size = 1;

        this.context.strokeStyle = '#333333';
        this.context.lineWidth = 1;
        this.context.lineJoin = 0;

        this.context.beginPath();
        for (var x = 0; x < WIDTH; x += GRID)
            for (var y = 0; y < HEIGHT; y += GRID) {
                this.context.moveTo(x - size, y);
                this.context.lineTo(x + size, y);
                this.context.moveTo(x, y - size);
                this.context.lineTo(x, y + size);
            }
        this.context.closePath();
        this.context.stroke();
    };
}

function CursorDrawer(context) {
    this.context = context;

    this.draw = function () {
        px = mouse.x; //+ GRID / 2;
        py = mouse.y; //+ GRID / 2;

        const size = 7;
        enableGlow(this.context, 'white',40);
        this.context.lineWidth = 2;
        this.context.strokeStyle = '#FFCCFF';

        this.context.beginPath();
        this.context.moveTo(concateValue(px) - size, concateValue(py) - size);
        this.context.lineTo(concateValue(px) + size, concateValue(py) - size);
        this.context.lineTo(concateValue(px) + size, concateValue(py) + size);
        this.context.lineTo(concateValue(px) - size, concateValue(py) + size);
        this.context.lineTo(concateValue(px) - size, concateValue(py) - size);
        this.context.closePath();
        this.context.stroke();

        disableGlow(this.context);

//        const crossSize = 10
//        this.context.beginPath();
//        this.context.lineWidth = 1;
//        this.context.strokeStyle = 'white';
//        this.context.moveTo(px - crossSize, py);
//        this.context.lineTo(px + crossSize, py);
//        this.context.moveTo(px, py - crossSize);
//        this.context.lineTo(px, py + crossSize);
//        this.context.closePath();

//        this.context.font = 'italic 8px Calibri';
//        this.context.fillStyle = '#999999';
//        this.context.fillText(concateValue(mx) + ',' + concateValue(my), mx + 4, my + 2);

//        this.context.stroke();
    };
}

function GetColorForName(name) {
    var c = parseInt(name) % 11;

    var colors = {
        0:'#33CC00',
        1:'#99FF66',
        2:'#CCFFCC',
        3:'#0099CC',
        4:'#009999',
        5:'#66FFFF',
        6:'#CCFFFF',
        7:'#CCFF00',
        8:'#FFFF66',
        9:'#FFFFCC',
        10:'#FF0033',
        11:'#CC0033',
        12:'#FF6699',
        13:'#FFCCCC'
    };
    var color = colors[c];
    return color
}

function StickDrawer(context) {
    this.context = context;

    this.draw = function (positions, name) {

        this.context.lineWidth = 3;
        this.context.strokeStyle = GetColorForName(name);

        enableGlow(context, GetColorForName(name), 35);

        this.context.beginPath();
        for (var i in positions) {
            var stick = positions[i];
            var startx = stick['start'][0];
            var starty = stick['start'][1];
            var endx = stick['end'][0];
            var endy = stick['end'][1];

            this.context.moveTo(startx, starty);
            this.context.lineTo(endx, endy);
        }
        this.context.closePath();
        this.context.stroke();

        disableGlow(context);
    };
}

function HomePointDrawer(context) {
    this.context = context;

    this.draw = function (position, name) {

        this.context.fillStyle = GetColorForName(name); //'rgba(255, 100, 0, 200)';
        this.context.strokeStyle = 'rgba(255, 255, 255, 50)';

        this.context.beginPath();
        this.context.arc(position[0], position[1], 10, 0, 2 * Math.PI);
        this.context.closePath();

        this.context.fill();
        this.context.stroke();

        this.context.font = '10px';
        this.context.fillStyle = 'white';
        this.context.fillText(name, position[0] - 1, position[1] - 1);
        this.context.fillStyle = 'black';
        this.context.fillText(name, position[0] + 1, position[1] + 1);
    };
}

function HeadPointDrawer(context) {
    this.context = context;

    this.draw = function (position, name) {
        if (HeadPointDrawer.frame < 4) {
            HeadPointDrawer.frame += 0.05;
        }
        else {
            HeadPointDrawer.frame = 2;
        }

        var headColor = GetColorForName(name);
        enableGlow(this.context, headColor, 25);

        this.context.fillStyle = headColor;
        this.context.strokeStyle = headColor; //'rgba(255, 255, 255, 50)';

        this.context.beginPath();
        this.context.arc(position[0], position[1], HeadPointDrawer.frame + 2, 90, 2 * Math.PI - 90);
        this.context.closePath();

        this.context.fill();
        this.context.stroke();

        disableGlow(this.context);
    };
}