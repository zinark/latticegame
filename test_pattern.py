from unittest import TestCase
from latticegame.Lattices import Lattice
from latticegame.Patterns import Pattern

class TestPattern(TestCase):
    def test___init__(self):
        lat = Lattice()\
        .addStickFromPos(0, 0, 1, 1)\
        .addStickFromPos(1, 1, 2, 0)\
        .addStickFromPos(2, 0, 1,0)\
        .addStickFromPos(1, 0, 0, 0)

        trianglePattern = Pattern(lat, 100, 'img.png')

        self.assertEquals (4, len (trianglePattern.lattice.sticks))
        self.assertEquals (100, trianglePattern.score)
        self.assertEquals ('img.png', trianglePattern.imageurl)
