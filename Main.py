import sys
import web
from web.session import Session, DiskStore
from latticegame import Config
from latticegame.Repositories import LatticeRepository
from latticegame.UserSession import UserSession

urls = (
    '/', 'Controllers.index',
    '/addStick/\((\d+),(\d+)\)-\((\d+),(\d+)\)', 'Controllers.addStick',
    '/headPoint', 'Controllers.headPoint',
    '/sync', 'Controllers.sync',
    '/version', 'Controllers.version'
    )
# a test commit for julython 2
if __name__ == '__main__':
    Config.version = 9.0
    sys.argv.append('21000')
    app = web.application(urls, locals())
    web.config.session_parameters['cookie_name'] = 'session_id'
    web.config.session_parameters['secret_key'] = 'fLjQfxqXt3NoIld40A0J'
    session = Session(app, DiskStore('sessions'))
    # {'user_id': 0, 'lattice_id': 0}
    # not working
    #    session['user_id'] = 0
    #    session['lattice_id'] = 0
    def pre_request():
        web.ctx.session = session
        web.ctx.userSession = UserSession(session, LatticeRepository())

    def post_request():
        pass

    app.add_processor(web.loadhook(pre_request))
    app.add_processor(web.unloadhook(post_request))
    app.run()