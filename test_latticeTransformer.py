from unittest import TestCase
from latticegame.Lattices import LatticeTransformer, Lattice

__docformat__ = 'restructuredtext en'
__author__ = 'A'

class TestLatticeTransformer(TestCase):
    def test_transform(self):
        lattice = Lattice().addStickFromPos(0, 0, 1, 1).addStickFromPos(0, 0, 1, 0)
        actual = LatticeTransformer(lattice).transform((5,5))
        self.assertEquals (actual.sticks[0].startPos, (5,5))
        self.assertEquals (actual.sticks[0].endPos, (6,6))
        self.assertEquals (actual.sticks[1].startPos, (5,5))
        self.assertEquals (actual.sticks[1].endPos, (6,5))