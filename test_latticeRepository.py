from unittest import TestCase
from latticegame.Lattices import Lattice
from latticegame.Repositories import LatticeRepository

class TestLatticeRepository(TestCase):
    def test_get(self):
        rep = LatticeRepository ()
        l1 = Lattice ()
        l1.id = 5
        rep.add(l1)

        self.assertEquals (rep.get(5), l1)

    def test_add(self):
        rep = LatticeRepository ()
        rep.add(Lattice ())
        rep.add(Lattice ())
        rep.add(Lattice ())
        self.assertEquals(3, rep.count())
