import json
from jinja2.environment import Environment
from jinja2.loaders import PackageLoader
import web
from latticegame.Lattices import  Stick, LatticeCropper, StickMovementValidator, Lattice, BattleModeValidator, LatticeTransformer
from latticegame.Patterns import PatternFinder, Pattern
from latticegame.Projections import Projector, ProjectionSpace
from latticegame import Config, Patterns
from latticegame.Repositories import LatticeRepository

templates = Environment(loader=PackageLoader('latticegame', '/templates'))
loadedPatterns = Patterns.PatternLoader().Load()

class index:
    def GET(self):
        return templates.get_template('index.html').render()


class version:
    def GET(self):
        return 'version = ' + str(Config.version)


class headPoint:
    def POST(self):
        currentLattice = web.ctx.userSession.lattice
        assert isinstance(currentLattice, Lattice)
        return json.dumps(getProjector().project(currentLattice.headPoint))


def cropLattice(lattice):
    return LatticeCropper(lattice, (-20, -20), (20, 20)).crop()


def getProjector():
    return Projector(ProjectionSpace(-20, -20, 20, 20).VerticalMirror(), ProjectionSpace(0, 0, 800, 800))


def prepForJson(patternsByPos):
    result = {}
    for pos in patternsByPos:
        pattern = patternsByPos[pos]
        assert isinstance(pattern, Pattern)
        # result.append([pattern.name, LatticeTransformer (pattern.lattice).transform(pos)])
        if not result.__contains__(pattern.name): result[pattern.name] = list()
        for pos in LatticeTransformer(pattern.lattice).transform(pos).positions:
            result[pattern.name].append(pos)

    return result


def getSyncResultItem(lattice):
    resultItem = {}
    if not len(lattice.sticks): return json.dumps(None)
    if lattice.homePoint is None: return json.dumps(None)
    if lattice.headPoint is None: return json.dumps(None)
    ptor = getProjector()

    croppedLattice = cropLattice(lattice)
    # foundPatterns = PatternFinder(loadedPatterns).Find(croppedLattice) # Calculate when add stick not here!

    posList = []
    for stick in croppedLattice.sticks:
        posList.append({
            'start': ptor.project(stick.startPos),
            'end': ptor.project(stick.endPos)
        })

    resultItem['name'] = str(lattice.id)
    resultItem['home'] = ptor.project((lattice.homePoint[0], lattice.homePoint[1]))
    resultItem['head'] = ptor.project((lattice.headPoint[0], lattice.headPoint[1]))
    resultItem['pos'] = posList
    # resultItem['patterns'] = prepForJson(foundPatterns)
    return resultItem


class sync:
    def POST(self):                             # when no change, return null
        result = []
        for lattice in LatticeRepository.all:
            result.append(getSyncResultItem(lattice))
        return json.dumps(result)


class addStick:
    def POST(self, x1, y1, x2, y2):
        currentLattice = web.ctx.userSession.lattice
        assert isinstance(currentLattice, Lattice)

        projector = Projector(ProjectionSpace(0, 0, 800, 800), ProjectionSpace(-20, -20, 20, 20).VerticalMirror())
        start = projector.project((int(x1), int(y1)))
        end = projector.project((int(x2), int(y2)))
        newStick = Stick(start, end)

        def strengthOfDestruction (sticks):
            if len(sticks) > 3: return len(sticks) - 3
            return 3

        for lattice in LatticeRepository().allExcept(currentLattice.id):
            assert isinstance(lattice, Lattice)
            if BattleModeValidator().Validate(currentLattice, cropLattice(lattice), start, end):
                strength = strengthOfDestruction (currentLattice.sticks)
                lattice.destroySticks(end, strength)
                currentLattice.destroySticks(end, strength)

        if StickMovementValidator().Validate(cropLattice(currentLattice), newStick):
            currentLattice.addStick(newStick)
        else:
            raise Exception('can not add stick : ' + str(newStick))

        result = []
        for lattice in LatticeRepository.all:
            result.append(getSyncResultItem(lattice))
        return json.dumps(result)

