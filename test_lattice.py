from unittest import TestCase
from latticegame.Lattices import Lattice, Stick

class TestLattice(TestCase):
    def test_addStickToTail1(self):
        s1 = Stick((0, 0), (1, 1))
        s2 = Stick((1, 1), (2, 2))
        lat = Lattice().addStick(s1).addStick(s2)

        self.assertEquals(s1, lat.sticks[0])
        self.assertEquals(s2, lat.sticks[1])
        self.assertEquals(2, len(lat.sticks))


    def test_relatedSticks(self):
        lat = Lattice().addStickFromPos(0, 0, 1, 1).addStickFromPos(1, 0, 1, 1)
        newStick = Stick((1, 1), (2, 2))
        lat.addStick(newStick)
        print newStick.relatedSticks
        self.assertEquals(2, len(newStick.relatedSticks))

    def test_addStickToTail2(self):
        s1 = Stick((0, 0), (1, 1))
        s2 = Stick((1, 1), (2, 2))

        s3 = Stick((1, 1), (2, 0))
        s4 = Stick((1, 1), (0, 2))
        lat = Lattice().addStick(s1).addStick(s2).addStick(s3).addStick(s4)

        self.assertEquals(s1, lat.sticks[0])
        self.assertEquals(s2, lat.sticks[1])
        self.assertEquals(s3, lat.sticks[2])
        self.assertEquals(s4, lat.sticks[3])
        self.assertEquals(4, len(lat.sticks))

    def test_homePoint(self):
        s1 = Stick((0, 0), (1, 1))
        s2 = Stick((1, 1), (2, 2))
        lat = Lattice().addStick(s1).addStick(s2)
        self.assertEquals((0, 0), lat.homePoint)
        self.assertEquals((2, 2), lat.headPoint)

    def test_destroySticks(self):
        lat = Lattice()\
        .addStick(Stick((0, 0), (1, 1)))\
        .addStick(Stick((1, 1), (2, 2)))\
        .addStick(Stick((2, 2), (3, 3)))\
        .addStick(Stick((3, 3), (4, 4)))\
        .addStick(Stick((4, 4), (5, 5)))

        lat.destroySticks((2, 2), 2)
        print lat
        self.assertEquals(3, len(lat.sticks))


    def test_positions(self):
        lat = Lattice()\
        .addStick(Stick((0, 0), (1, 1)))\
        .addStick(Stick((1, 1), (2, 2)))\
        .addStick(Stick((2, 2), (3, 3)))\
        .addStick(Stick((3, 3), (4, 4)))\
        .addStick(Stick((4, 4), (5, 5)))

        print lat.positions()
        self.assertEquals(len(lat.positions()), 6)

    def makeLattice(self):
        lat = Lattice()\
        .addStickFromPos(0, 0, 1, 0)\
        .addStickFromPos(1, 0, 1, 1)\
        .addStickFromPos(1, 1, 2, 1)\
        .addStickFromPos(2, 1, 2, 2)\
        .addStickFromPos(2, 2, 3, 2)\
        .addStickFromPos(3, 2, 3, 3)\
        .addStickFromPos(3, 3, 4, 3)\
        .addStickFromPos(4, 3, 4, 4)
        return lat

    def test_linkedSticks(self):
        lattice = self.makeLattice()
        print lattice.linkedSticks((0,0))

    def test_linkedSticks2(self):
        lattice = self.makeLattice()
        print lattice.linkedSticks((2,2))


    def test_destroySticks_forStrength2(self):
        lattice = self.makeLattice()
        lattice.destroySticks((2, 2), 2)
        self.assertEquals(6, len(lattice.sticks))

    def test_destroySticks_forStrengthLengthOfLattice(self):
        lattice = self.makeLattice()
        lattice = lattice.destroySticks((2, 2), len(lattice.sticks))
        self.assertEquals(0, len(lattice.sticks))

    def test_destroySticks_forStrengthLengthOfLatticeMinusOne(self):
        lattice = self.makeLattice()
        lattice = lattice.destroySticks((2, 2), len(lattice.sticks) - 1)
        self.assertEquals(1, len(lattice.sticks))
