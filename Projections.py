import copy

class ProjectionSpace(object):
    def __init__(self, x0, y0, x1, y1):
        assert x0 < x1
        assert y0 < y1
        self.x0 = x0
        self.y0 = y0
        self.x1 = x1
        self.y1 = y1
        self.verticalMirrored = False

    def VerticalMirror(self):
        self.verticalMirrored = True
        return self

    def __repr__(self):
        return 'ProjectionSpace ({0}, {1} to {2}, {3})'.format(self.x0, self.y0, self.x1, self.y1)


class Projector:
    def __init__(self, inputSpace, outputSpace):
        assert isinstance(inputSpace, ProjectionSpace)
        assert isinstance(outputSpace, ProjectionSpace)
        self.inputSpace = inputSpace
        self.outputSpace = outputSpace

    def project(self, pos):
        assert isinstance(pos, tuple)
        assert len(pos) == 2

        def findDelta(space):
            assert isinstance(space, ProjectionSpace)
            return 0 - space.x0, 0 - space.y0

        def MoveSpace(space, delta):
            assert isinstance(space, ProjectionSpace)
            space.x0 += delta[0]
            space.y0 += delta[1]
            space.x1 += delta[0]
            space.y1 += delta[1]

        iSpace = copy.copy(self.inputSpace)
        delta = findDelta(iSpace)
        MoveSpace(iSpace, delta)

        outWidth = abs(self.outputSpace.x1 - self.outputSpace.x0)
        outHeight = abs(self.outputSpace.y1 - self.outputSpace.y0)

        inWidth = abs(iSpace.x1 - iSpace.x0)
        inHeight = abs(iSpace.y1 - iSpace.y0)

        rHorizontal = float(outWidth) / float(inWidth)
        rVertical = float(outHeight) / float(inHeight)

        pos = pos[0] + delta[0], pos[1] + delta[1]
        x = int(pos[0] * rHorizontal + self.outputSpace.x0)
        y = int(pos[1] * rVertical + self.outputSpace.y0)

        if self.inputSpace.verticalMirrored:
            return x, outHeight - y

        if self.outputSpace.verticalMirrored:
            return x, -y

        return x, y