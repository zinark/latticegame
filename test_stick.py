from unittest import TestCase
from latticegame.Lattices import Stick

class TestStick(TestCase):
    def test___init__(self):
        s = Stick((0, 0), (1, 1))
        self.assertEquals(s.endPos, (1, 1))
        self.assertEquals(s.startPos, (0, 0))

    def test_reverse(self):
        r = Stick((1, 1), (-1, -1)).reverse()
        self.assertEquals((-1, -1), r.startPos)
        self.assertEquals((1, 1), r.endPos)

    def test_equality(self):
        s1 = Stick((1, 1), (2, 2))
        s2 = Stick((1, 1), (2, 2))
        self.assertEquals(s1, s2)