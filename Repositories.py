from latticegame.Models import User

class LatticeRepository:
    all = list()

    def get(self, latticeId):
        return [l for l in LatticeRepository.all if l.id == latticeId][0]

    def add(self, lattice):
        LatticeRepository.all.append(lattice)

    def count(self):
        return len(LatticeRepository.all)


#class UserRepository:
#    container = list()
#
#    def get(self, id):
#        return [usr for usr in UserRepository.container if usr.id == id][0]
#
#    def add(self, lattice):
#        UserRepository.container.append(lattice)
#
#    def count(self):
#        return len(UserRepository.container)

    def allExcept(self, latticeId):
        return [l for l in LatticeRepository.all if l.id != latticeId]